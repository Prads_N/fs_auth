﻿namespace FinalAuth.Models.Data
{
    public class ClientAllowedScopeData
    {
        public ulong Id { get; set; }
        public string Scope { get; set; }
    }
}