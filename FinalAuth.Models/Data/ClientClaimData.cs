﻿namespace FinalAuth.Models.Data
{
    public class ClientClaimData
    {
        public ulong Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}