﻿namespace FinalAuth.Models.Data
{
    public class ApiResourceData
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }
}