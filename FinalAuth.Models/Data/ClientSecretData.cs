﻿namespace FinalAuth.Models.Data
{
    public class ClientSecretData
    {
        public ulong Id { get; set; }
        public string Secret { get; set; }
    }
}