﻿namespace FinalAuth.Models.Data
{
    using System.Collections.Generic;

    public class ClientData
    {
        public ulong Id { get; set; }
        public string ClientId { get; set; }
        public IEnumerable<ClientAllowedScopeData> Scopes { get; set; }
        public IEnumerable<ClientClaimData> Claims { get; set; }
        public IEnumerable<ClientGrantTypeData> GrantTypes { get; set; }
        public IEnumerable<ClientSecretData> Secrets { get; set; }
    }
}