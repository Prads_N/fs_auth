﻿namespace FinalAuth.Models.Data
{
    public class ClientGrantTypeData
    {
        public ulong Id { get; set; }
        public string GrantType { get; set; }
    }
}