﻿namespace FinalAuth.Models.Contracts.Data
{
    using FinalAuth.Models.Data;
    using System.Threading.Tasks;

    public interface IClientRepository
    {
        Task<ClientData> GetClientById(string clientId);
        Task Insert(ClientData clientData);
    }
}