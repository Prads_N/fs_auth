﻿namespace FinalAuth.Models.Contracts.Data
{
    using FinalAuth.Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IApiResourceRepository
    {
        Task<ApiResourceData> GetApiResource(string name);
        Task<IEnumerable<ApiResourceData>> GetApiResources(IEnumerable<string> names);
        Task<IEnumerable<ApiResourceData>> GetAll();
    }
}