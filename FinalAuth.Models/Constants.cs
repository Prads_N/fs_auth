﻿namespace FinalAuth.Models
{
    public static class Constants
    {
        public static class DatabaseTables
        {
            public static string Clients = "clients";
            public static string ClientSecrets = "client_secrets";
            public static string ClientGrantTypes = "client_grant_types";
            public static string ClientAllowedScopes = "client_allowed_scopes";
            public static string ClientClaims = "client_claims";
            public static string ApiResources = "api_resources";
        }

        public static class Auth
        {
            public static int MaxSecretLength = 128;
        }
    }
}