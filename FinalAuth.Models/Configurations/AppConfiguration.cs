﻿namespace FinalAuth.Models.Configurations
{
    public class AppConfiguration
    {
        public DbConnectionStrings DbConnectionStrings { get; set; }
        public Certificate Certificate { get; set; }
    }
}