﻿namespace FinalAuth.Models.Configurations
{
    public class Certificate
    {
        public string KeyStoreIssuer { get; set; }
    }
}