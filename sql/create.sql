CREATE DATABASE IF NOT EXISTS `final_auth`;
USE `final_auth`;

CREATE TABLE `clients` (
	`Id`					BIGINT UNSIGNED AUTO_INCREMENT,
	`ClientId`				VARCHAR(256) NOT NULL,
	
	`DateCreated`			DATETIME NOT NULL,
	`Deleted`				BIT NOT NULL DEFAULT 0,
	
	PRIMARY KEY (`Id`),
	UNIQUE INDEX `idx_clientid` (`ClientId`)
)engine=innodb default charset=utf8mb4;

CREATE TABLE `client_secrets` (
	`Id`				BIGINT UNSIGNED NOT NULL,
	`Secret`		VARCHAR(256) NOT NULL,
	
	PRIMARY KEY (`Id`, `Secret`)
)engine=innodb default charset=utf8mb4;

CREATE TABLE `client_grant_types` (
	`Id`				BIGINT UNSIGNED NOT NULL,
	`GrantType`			VARCHAR(256) NOT NULL,
	
	PRIMARY KEY (`Id`, `GrantType`)
)engine=innodb default charset=utf8mb4;

CREATE TABLE `client_allowed_scopes` (
	`Id`				BIGINT UNSIGNED NOT NULL,
	`Scope`				VARCHAR(256) NOT NULL,
	
	PRIMARY KEY (`Id`, `Scope`)
)engine=innodb default charset=utf8mb4;

CREATE TABLE `client_claims` (
	`Id`				BIGINT UNSIGNED NOT NULL,
	`Key`				VARCHAR(256) NOT NULL,
	`Value`				VARCHAR(256) NOT NULL,
	
	PRIMARY KEY (`Id`,`Key`)
)engine=innodb default charset=utf8mb4;

CREATE TABLE `api_resources` (
	`Name`				VARCHAR(256) NOT NULL,
	`DisplayName`		VARCHAR(256) NOT NULL,
	`Deleted`			BIT NOT NULL DEFAULT 0,
	
	PRIMARY KEY (`Name`)
)engine=innodb default charset=utf8mb4;