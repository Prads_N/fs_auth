﻿namespace FinalAuth.Data
{
    using System.Threading.Tasks;
    using Models.Contracts.Data;
    using IdentityServer4.Models;
    using IdentityServer4.Stores;
    using System.Linq;
    using FinalAuth.Utils.Extensions;
    using System.Security.Claims;

    public class ClientStore : IClientStore
    {
        private readonly IClientRepository _clientRepository;

        public ClientStore(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task<Client> FindClientByIdAsync(string clientId)
        {
            var clientData = await _clientRepository.GetClientById(clientId);

            if (clientData == null)
                return null;

            return new Client
            {
                ClientId = clientData.ClientId,
                AllowedGrantTypes = clientData.GrantTypes.Select(gt => gt.GrantType).AsCollection(),
                ClientSecrets = clientData.Secrets.Select(cs => new Secret(cs.Secret)).AsCollection(),
                AllowedScopes = clientData.Scopes.Select(s => s.Scope).AsCollection(),
                Claims = clientData.Claims.Select(c => new Claim(c.Key, c.Value)).AsCollection(),
                ClientClaimsPrefix = "",
                AccessTokenLifetime = 2000
            };
        }
    }
}