﻿namespace FinalAuth.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FinalAuth.Models.Contracts.Data;
    using IdentityServer4.Models;
    using IdentityServer4.Stores;

    public class ResourceStore : IResourceStore
    {
        private readonly IApiResourceRepository _apiResourceRepository;

        public ResourceStore(IApiResourceRepository apiResourceRepository)
        {
            _apiResourceRepository = apiResourceRepository;
        }

        public async Task<ApiResource> FindApiResourceAsync(string name)
        {
            var data = await _apiResourceRepository.GetApiResource(name);

            if (data == null)
                return null;

            return new ApiResource(name, data.DisplayName);
        }

        public async Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            //Currently the api name is the scope but this needs to change in the future
            var data = await _apiResourceRepository.GetApiResources(scopeNames);

            if (data == null)
                return null;

            return data.Select(d => new ApiResource(d.Name, d.DisplayName));
        }

        public Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return Task.FromResult(Enumerable.Empty<IdentityResource>());
        }

        public async Task<Resources> GetAllResourcesAsync()
        {
            var apiResources = await _apiResourceRepository.GetAll();

            return new Resources(Enumerable.Empty<IdentityResource>(), apiResources.Select(d => new ApiResource(d.Name, d.DisplayName)));
        }
    }
}