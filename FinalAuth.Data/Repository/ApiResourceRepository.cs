﻿namespace FinalAuth.Data.Repository
{
    using Primitives;
    using Models.Contracts.Data;
    using System.Threading.Tasks;
    using FinalAuth.Models;
    using FinalAuth.Models.Data;
    using System.Collections.Generic;

    public class ApiResourceRepository : BaseRepository, IApiResourceRepository
    {
        public ApiResourceRepository(IDbConnectionFactory dbConnectionFactory) 
            : base(dbConnectionFactory) { }

        public Task<ApiResourceData> GetApiResource(string name)
        {
            var sql = $"SELECT * FROM {Constants.DatabaseTables.ApiResources} WHERE `Name`=@name AND Deleted=0;";
            return QuerySingleOrDefault<ApiResourceData>(sql, new { name });
        }

        public Task<IEnumerable<ApiResourceData>> GetApiResources(IEnumerable<string> names)
        {
            var sql = $"SELECT * FROM {Constants.DatabaseTables.ApiResources} WHERE `Name` IN @names AND Deleted=0;";
            return Query<ApiResourceData>(sql, new { names });
        }

        public Task<IEnumerable<ApiResourceData>> GetAll()
        {
            return Query<ApiResourceData>($"SELECT * FROM {Constants.DatabaseTables.ApiResources} WHERE Deleted=0", null);
        }
    }
}