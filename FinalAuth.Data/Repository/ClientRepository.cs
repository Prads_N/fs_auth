﻿namespace FinalAuth.Data.Repository
{
    using FinalAuth.Models;
    using FinalAuth.Models.Data;
    using Models.Contracts.Data;
    using Primitives;
    using System.Linq;
    using System.Threading.Tasks;

    public class ClientRepository : BaseRepository, IClientRepository
    {
        public ClientRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory) { }

        public async Task<ClientData> GetClientById(string clientId)
        {
            var clientSql = $"SELECT * FROM {Constants.DatabaseTables.Clients} WHERE `ClientId`=@clientId;";
            var client = await QuerySingleOrDefault<ClientData>(clientSql, new { clientId });

            if (client == null)
                return null;

            var scopesTask = Query<ClientAllowedScopeData>(
                $"SELECT * FROM {Constants.DatabaseTables.ClientAllowedScopes} WHERE `Id`=@Id;", 
                new { client.Id });

            var claimsTask = Query<ClientClaimData>(
                $"SELECT * FROM {Constants.DatabaseTables.ClientClaims} WHERE `Id`=@Id;",
                new { client.Id });

            var grantTypesTask = Query<ClientGrantTypeData>(
                $"SELECT * FROM {Constants.DatabaseTables.ClientGrantTypes} WHERE `Id`=@Id;",
                new { client.Id });

            var secrectsTask = Query<ClientSecretData>(
                $"SELECT * FROM {Constants.DatabaseTables.ClientSecrets} WHERE `Id`=@Id;",
                new { client.Id });

            client.Claims = await claimsTask;
            client.Scopes = await scopesTask;
            client.GrantTypes = await grantTypesTask;
            client.Secrets = await secrectsTask;

            return client;
        }

        public async Task Insert(ClientData clientData)
        {
            var clientSql = $@"INSERT INTO {Constants.DatabaseTables.Clients} (`ClientId`,`DateCreated`) VALUES (@ClientId,UTC_TIMESTAMP());
                                SELECT LAST_INSERT_ID();";

            clientData.Id = await WriteAndQuerySingleOrDefault<ulong>(clientSql, new { clientData.ClientId });

            if (clientData.Claims.Any())
            {
                foreach (var claim in clientData.Claims)
                    claim.Id = clientData.Id;

                var claimsSql = $"INSERT INTO {Constants.DatabaseTables.ClientClaims} (`Id`,`Key`,`Value`) VALUES (@Id,@Key,@Value);";
                await Execute(claimsSql, clientData.Claims);
            }

            if (clientData.Secrets.Any())
            {
                foreach (var secret in clientData.Secrets)
                    secret.Id = clientData.Id;

                var secretSql = $"INSERT INTO {Constants.DatabaseTables.ClientSecrets} (`Id`,`Secret`) VALUES (@Id,@Secret);";
                await Execute(secretSql, clientData.Secrets);
            }

            if (clientData.Scopes.Any())
            {
                foreach (var scope in clientData.Scopes)
                    scope.Id = clientData.Id;

                var scopeSql = $"INSERT INTO {Constants.DatabaseTables.ClientAllowedScopes} (`Id`,`Scope`) VALUES (@Id,@Scope);";
                await Execute(scopeSql, clientData.Scopes);
            }

            if (clientData.GrantTypes.Any())
            {
                foreach (var grantType in clientData.GrantTypes)
                    grantType.Id = clientData.Id;

                var grantSql = $"INSERT INTO {Constants.DatabaseTables.ClientGrantTypes} (`Id`,`GrantType`) VALUES (@Id,@GrantType);";
                await Execute(grantSql, clientData.GrantTypes);
            }
        }
    }
}