﻿namespace FinalAuth.App
{
    using FinalAuth.App.Extensions;
    using FinalAuth.Data;
    using FinalAuth.Data.Repository;
    using FinalAuth.Models;
    using FinalAuth.Models.Configurations;
    using FinalAuth.Models.Contracts.Data;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = _configuration.Get<AppConfiguration>();

            services.AddIdentityServer(o => o.InputLengthRestrictions.ClientSecret = Constants.Auth.MaxSecretLength)
                .AddResourceStore<ResourceStore>()
                .AddClientStore<ClientStore>()
                .AddSigingCredentialFromKeyStore(config.Certificate.KeyStoreIssuer);
            
            services.AddSingleton<IDbConnectionFactory>(new DbConnectionFactory(config.DbConnectionStrings.Read, config.DbConnectionStrings.Write));
            services.AddSingleton<IApiResourceRepository, ApiResourceRepository>();
            services.AddSingleton<IClientRepository, ClientRepository>();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseIdentityServer();
        }
    }
}