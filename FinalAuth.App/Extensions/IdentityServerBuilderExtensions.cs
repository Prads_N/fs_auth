﻿namespace FinalAuth.App.Extensions
{
    using Microsoft.Extensions.DependencyInjection;
    using System.Security.Cryptography.X509Certificates;

    public static class IdentityServerBuilderExtensions
    {
        public static IIdentityServerBuilder AddSigingCredentialFromKeyStore(this IIdentityServerBuilder builder, string KeyStoreIssuer)
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);

            var certificates = store.Certificates.Find(X509FindType.FindByIssuerName, KeyStoreIssuer, true);

            builder.AddSigningCredential(certificates[0]);
            return builder;
        }
    }
}