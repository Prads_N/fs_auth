﻿namespace FinalAuth.Utils.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    public static class EnumerableExtensions
    {
        public static ICollection<T> AsCollection<T>(this IEnumerable<T> source)
        {
            return source as ICollection<T> ?? source.ToList();
        }
    }
}