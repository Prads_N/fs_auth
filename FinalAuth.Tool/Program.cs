﻿namespace FinalAuth.Tool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FinalAuth.Tool.Commands.Contracts;
    using Microsoft.Extensions.DependencyInjection;

    public class Program
    {
        public static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            Dependency.RegisterAll(serviceCollection);

            await ExecuteCommands(serviceCollection.BuildServiceProvider().GetServices<ICommand>());
        }

        private static async Task ExecuteCommands(IEnumerable<ICommand> commandList)
        {
            var commands = commandList.ToDictionary(c => c.Command, StringComparer.OrdinalIgnoreCase);

            while (true)
            {
                Console.Write("-> ");
                var command = Console.ReadLine();

                if (command.Equals("exit", StringComparison.OrdinalIgnoreCase))
                    break;

                if (commands.TryGetValue(command, out var cmd))
                    await cmd.Execute();
            }
        }
    }
}
