﻿namespace FinalAuth.Tool
{
    using Data;
    using Data.Repository;
    using Models.Contracts.Data;
    using Commands;
    using Commands.Contracts;
    using Microsoft.Extensions.DependencyInjection;
    using Services.Contracts;
    using Services;

    internal static class Dependency
    {
        public static void RegisterAll(IServiceCollection services)
        {
            services.AddSingleton<IDbConnectionFactory>(
                new DbConnectionFactory("Server=final.cq7pba1225e2.ap-southeast-2.rds.amazonaws.com; Port=3306; Database=final_auth; Uid=admin; Pwd=vMjVMUAjnvDOT4uCgGh9; charset=utf8mb4;",
                "Server=final.cq7pba1225e2.ap-southeast-2.rds.amazonaws.com; Port=3306; Database=final_auth; Uid=admin; Pwd=vMjVMUAjnvDOT4uCgGh9; charset=utf8mb4;"));
            
            services.AddSingleton<IClientRepository, ClientRepository>();
            services.AddSingleton<ICommand, NewClient>();
            services.AddSingleton<ICommand, NewCustomer>();

            services.AddSingleton<IClientService, ClientService>();
        }
    }
}
