﻿namespace FinalAuth.Tool.Services.Contracts
{
    using System.Threading.Tasks;

    internal interface IClientService
    {
        Task CreateNewClient(string clientId, string grantType, string scopes, string claims);
    }
}