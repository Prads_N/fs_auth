﻿namespace FinalAuth.Tool.Services
{
    using Contracts;
    using FinalAuth.Models;
    using FinalAuth.Models.Data;
    using IdentityServer4.Models;
    using Models.Contracts.Data;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Threading.Tasks;

    internal class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;

        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task CreateNewClient(string clientId, string grantType, string scopes, string claims)
        {
            //Secret
            var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            byte[] secretBytes = new byte[Constants.Auth.MaxSecretLength / 2];

            rngCryptoServiceProvider.GetBytes(secretBytes, 0, Constants.Auth.MaxSecretLength / 2);
            var secret = BitConverter.ToString(secretBytes).Replace("-", "");

            //Create client data
            var client = new ClientData();

            client.ClientId = clientId;

            client.GrantTypes = grantType.Split(',').Select(gt => new ClientGrantTypeData
            {
                GrantType = gt
            }).ToList();

            client.Scopes = scopes.Split(',').Select(s => new ClientAllowedScopeData
            {
                Scope = s
            }).ToList();

            if (String.IsNullOrWhiteSpace(claims))
            {
                client.Claims = new List<ClientClaimData>();
            }
            else
            {
                client.Claims = claims.Split(',').Select(c =>
                {
                    var keyValue = c.Split('|');
                    return new ClientClaimData
                    {
                        Key = keyValue[0],
                        Value = keyValue[1]
                    };
                }).ToList();
            }

            client.Secrets = new[] { new ClientSecretData { Secret = secret.Sha256() } };

            await _clientRepository.Insert(client);

            Console.WriteLine("New client created");
            Console.WriteLine($"Client Id: {clientId}");
            Console.WriteLine($"Client secret: {secret}");
        }
    }
}
