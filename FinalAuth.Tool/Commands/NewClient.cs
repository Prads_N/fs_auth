﻿namespace FinalAuth.Tool.Commands
{
    using System;
    using System.Threading.Tasks;
    using Contracts;
    using FinalAuth.Tool.Services.Contracts;

    internal class NewClient : ICommand
    {
        private readonly IClientService _clientService;

        public string Command => "newclient";

        public NewClient(IClientService clientService)
        {
            _clientService = clientService;
        }

        public Task Execute()
        {
            Console.WriteLine($"Executing new client command");

            //Client Id
            Console.Write("Enter client id: ");
            var clientId = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(clientId))
            {
                Console.WriteLine("Client id cannot be empty");
                return Task.CompletedTask;
            }

            //Grant Types
            Console.Write("Enter grant types (separated by comma): ");
            var grantType = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(grantType))
            {
                Console.WriteLine("Grant type cannot be empty");
                return Task.CompletedTask;
            }

            //Scopes
            Console.Write("Enter allowed scopes (separated by comma): ");
            var scopes = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(scopes))
            {
                Console.WriteLine("Scopes cannot be empty");
                return Task.CompletedTask;
            }

            //Claims
            Console.Write("Enter claims (separate key value pair with | and multiple claims by comma): ");
            var claims = Console.ReadLine();

            Console.Write("Are you sure you want to create this client? Enter n if you want to cancel: ");
            var yn = Console.ReadLine();

            if (yn.Equals("n", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Cancelling...");
                return Task.CompletedTask;
            }

            return _clientService.CreateNewClient(clientId, grantType, scopes, claims);
        }
    }
}