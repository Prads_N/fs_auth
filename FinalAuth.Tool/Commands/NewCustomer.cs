﻿namespace FinalAuth.Tool.Commands
{
    using System;
    using System.Threading.Tasks;
    using Contracts;
    using FinalAuth.Tool.Services.Contracts;

    internal class NewCustomer : ICommand
    {
        private readonly IClientService _clientService;

        public string Command => "newcustomer";

        public NewCustomer(IClientService clientService)
        {
            _clientService = clientService;
        }

        public Task Execute()
        {
            Console.WriteLine($"Executing new client command");

            //Client Id
            Console.Write("Enter client id: ");
            var clientId = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(clientId))
            {
                Console.WriteLine("Client id cannot be empty");
                return Task.CompletedTask;
            }

            //Customer Id
            Console.Write("Enter customer id: ");
            var customerId = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(clientId))
            {
                Console.WriteLine("Customer id cannot be empty");
                return Task.CompletedTask;
            }

            return _clientService.CreateNewClient(clientId, "client_credentials", "fs_client", $"customer_id|{customerId}");
        }
    }
}