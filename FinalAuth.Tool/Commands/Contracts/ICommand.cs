﻿namespace FinalAuth.Tool.Commands.Contracts
{
    using System.Threading.Tasks;

    internal interface ICommand
    {
        string Command { get; }
        Task Execute();
    }
}